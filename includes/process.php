<?php 
	// Check for Header Injections
	function has_header_injection($str) {
	return preg_match( "/[\r\n]/", $str );
	}

	if (isset($_POST['formcontact_submit'])) {
		// Assign trimmed form data to variables
		// Note that the value within the $_POST array is looking for the HTML "name" attribute, i.e. name="email"
		$name   = trim($_POST['name']);
		$email  = trim($_POST['email']);
		$phone  = trim($_POST['phone']);
		$msg    = $_POST['message']; // no need to trim message
		// Check to see if $name or $email have header injections
		
		if (has_header_injection($name) || has_header_injection($email)) {
			die(); // If true, kill the script
		}
		
		if (!$name || !$email || !$msg) {
			echo '<h4 class="error">All fields required.</h4><a class="page-scroll" href="../#contact">Tentar novamente!</a>';
			exit;
		}
		// Add the recipient email to a variable
		$to = "feedback@adotdog.com";
		// Create a subject
		$subject = "$name sent a message via your contact form";
		// Construct the message
		$message .= "Name: $name\r\n";
		$message .= "Email: $email\r\n\r\n";
		$message .= "Phone: $phone\r\n\r\n";
		$message .= "Message:\r\n$msg";
		// If the subscribe checkbox was checked
		
		if (isset($_POST['subscribe']) && $_POST['subscribe'] == 'Subscribe' ) {
			// Add a new line to the $message
			$message .= "\r\n\r\nPlease add $email to the mailing list.\r\n";
		}
		
		$message = wordwrap($message, 72); // Keep the message neat n' tidy
		// Set the mail headers into a variable
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
		$headers .= "From: " . $name . " <" . $email . ">\r\n";
		$headers .= "X-Priority: 1\r\n";
		$headers .= "X-MSMail-Priority: High\r\n\r\n";
		// Send the email!
		ini_set ( "SMTP", "mail.adotdog.com" );
		date_default_timezone_set('America/Sao_Paulo');

		imap_mail($to, $subject, $message, $headers);

	
?><!-- Show success message after email has sent -->
                
<!-- Small modal 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button> -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <h5>Obrigado por mandar seu contato para o AdotDog</h5>
                <p>Por favor, aguarde 24 horas para uma resposta.</p>
                <p><a href="../#contact" class="button block">&laquo; Recarregar a pagina</a></p>
    </div>
  </div>
</div>
<?php
    } else {
?>


<?php
        }
?>