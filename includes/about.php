<section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Sobre o AdotDog</h2>
                <p>O AdotDog foi criado com o simples objetivo de ajudar os peludos que ainda não tem um lar. Muitos deles
                    foram abandonados e <a href="#list" class="page-scroll">precisam de você</a>.
                    Adotar um animal é um ato de amor, e uma experiência única de companherismo.</p>
                <p>Se você representa uma ONG ou associação sem fins lucrativos e ainda não está em nossa lista, mande seu <a href="#contact" class="page-scroll">contato</a> para fazer parte também.
                   Juntos seremos mais fortes. Se você busca aqui adotar um fiel amigo veja nossos <a href="#list" class="page-scroll">parceiros</a>.</p>
                <p>Eles precisam de você e você deles. Faça a diferença, compartilhe o carinho e amor por quem sempre estará ao seu lado</p>
            </div>
        </div>
    </section>