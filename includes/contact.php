<!--      NOTE:
        In the form in contact.php, the name text field has the name "name"
        If the user submits the form, the $_POST['name'] variable will be
        automatically created, and will contain the text they typed into
        the field. The $_POST['email'] variable will contain whatever they typed
        into the email field.
    
    
        PHP used in this script:
        
        preg_match()
        - Perform a regular expression match
        - http://ca2.php.net/preg_match
        
        isset()
        - Determine if a variable is set and is not NULL
        - http://ca2.php.net/manual/en/function.isset.php
        
        $_POST
        - An associative array of variables passed to the current script via the HTTP POST method.
        - http://www.php.net/manual/en/reserved.variables.post.php
        
        trim()
        - Strip whitespace (or other characters) from the beginning and end of a string
        - http://www.php.net/manual/en/function.trim.php
        
        exit
        - Output a message and terminate the current script
        - http://www.php.net//manual/en/function.exit.php
        
        die()
        - Equivalent to exit
        - http://ca1.php.net/manual/en/function.die.php
        
        wordwrap()
        - Wraps a string to a given number of characters
        - http://ca1.php.net/manual/en/function.wordwrap.php
        
        mail()
        - Send mail
        - http://ca1.php.net/manual/en/function.mail.php -->   
<section id="contact" class="container content-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contato com a AdotDog</h2>
                <div class="col-md-12">
                    <div class="well well-sm">
                        <p>Sinta-se a vontade para enviar um e-mail e fornecer algum feedback sobre nossa página, ou sugestões de novos parceiros e Ongs que ainda não estão aqui, ou ainda que seja só para dizer Olá! :)</p>
                        
                        <p>Diretamente através do <a href="mailto:feedback@adotdog.com">feedback@adotdog.com</a>
                        <br></p>
                        <!-- Button trigger modal -->
                        <p>Ou através do <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">formulário</button></p>
                        <br>
                    </div>            
                </div>
            </div>
        </div>
   
        <?php include ('includes/modal.php'); ?>

        <div class="row">
                
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <!--<a href="https://twitter.com/" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>-->
                    </li>
                    <li>
                        <a href="https://www.facebook.com/AdotDog-216101805441566/" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
                    </li>
                    <li>
                        <!--<a href="https://plus.google.com/" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>-->
                    </li>
                </ul>
        </div>
         <style>
            .header {
                color: #36A0FF;
                font-size: 27px;
                padding: 10px;
            }

            .bigicon {
                font-size: 35px;
                color: #FFFFFF;
            }
        </style>
        
    </div>
</section>