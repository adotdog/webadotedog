<section id="destaque" class="">
        <div class="container">
            <div class="col-md-3 col-sm-6">
                <div class="thumbnail">
                    <img src="https://images.unsplash.com/photo-1455103493930-a116f655b6c5?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=eafc1b315f16c8abcbabf20010ac6f55" alt="">
                    <div class="caption">
                        <h5>Associação Protetora dos Animais de São Caetano do Sul</h5>
                        <p>Organização de proteção dos animais</p>
                        <p><i class="fa fa-phone"></i> (11) 4229-9004</p>
                        <p>
                            <a href="http://goo.gl/VihAly" class="btn btn-primary">Ajudar!</a> <a href="http://goo.gl/VihAly" class="btn btn-default">Mais Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="thumbnail">
                    <img src="https://images.unsplash.com/photo-1455526050980-d3e7b9b789a4?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=8d504e1c3f19d3487fb2a0e661813cd2" alt="">
                    <div class="caption">
                        <h5>Associação Global de Desenvolvimento Sustentado</h5>
                        <p>Non-Profit Organization</p>
                        <p><i class="fa fa-phone"></i> (11) 4365-1976</p>
                        <p>
                            <a href="https://goo.gl/ogEsCt" class="btn btn-primary">Ajudar!</a> <a href="https://goo.gl/ogEsCt" class="btn btn-default">Mais Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="thumbnail">
                    <img src="https://images.unsplash.com/18/pups.JPG?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=b0a6a54dc1dc0f7b7040a9cda36e6133" alt="">
                    <div class="caption">
                        <h5>ONG SOS Cidadania Animal - Clínica Veterinária Popular</h5>
                        <p>Causa</p>
                        <p><i class="fa fa-phone"></i> (11)2759-2010</p>
                        <p>
                            <a href="https://goo.gl/knPdUC" class="btn btn-primary">Ajudar!</a> <a href="https://goo.gl/knPdUC" class="btn btn-default">Mais Info</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="thumbnail">
                    <img src="https://images.unsplash.com/photo-1436990276129-47ab01d4e245?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=32244afe9e5c7aa8bc2f8f14fd9a1176" alt="">
                    <div class="caption">
                        <h5>Nome do Parceiro</h5>
                        <p>Telefone.</p>
                        <p><i class="fa fa-phone"></i> (11) 4094-2059</p>
                        <p>
                            <a href="#" class="btn btn-primary">Ajudar!</a> <a href="#" class="btn btn-default">Mais Info</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--Nova Div-->
        <div class="container">
        <div class="col-md-3 col-sm-6">
            <div class="thumbnail">
                <img src="https://images.unsplash.com/photo-1444212477490-ca407925329e?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=95cad14029d1c42693e73d9fe9124618" alt="">
                <div class="caption">
                    <h5>Nome do Parceiro</h5>
                    <p>Telefone.</p>
                    <p><i class="fa fa-phone"></i> (11) </p>
                    <p>
                        <a href="#" class="btn btn-primary">Ajudar!</a> <a href="#" class="btn btn-default">Mais Info</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 ">
            <div class="thumbnail">
                <img src="http://placehold.it/800x500" alt="">
                <div class="caption">
                    <h5>Nome do Parceiro</h5>
                    <p>Telefone.</p>
                    <p><i class="fa fa-phone"></i> (11) </p>
                    <p>
                        <a href="#" class="btn btn-primary">Ajudar!</a> <a href="#" class="btn btn-default">Mais Info</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnail">
                <img src="http://placehold.it/800x500" alt="">
                <div class="caption">
                    <h5>Amigo dos Bichos</h5>
                    <p>Pet Groomer</p>
                    <p><i class="fa fa-phone"></i> (11) 3297-8584</p>
                    <p>
                        <a href="#" class="btn btn-primary">Ajudar!</a> <a href="https://www.google.com.br/maps/dir/''/Av.+Senador+Casemiro+da+Rocha,+1021+-+Saúde,+São+Paulo+-+SP,+04047-000/data=!4m5!4m4!1m0!1m2!1m1!1s0x94ce5a47a423acd9:0xa0c054f4b51d8634?sa=X&ved=0ahUKEwi97tKo_dfMAhWBkJAKHZSSBeUQiBMIMjAE" class="btn btn-default">Mais Info</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnail">
                <img src="http://placehold.it/800x500" alt="">
                <div class="caption">
                    <h5>Associação de Amparo Aos Animais</h5>
                    <p>Association or Organization</p>
                    <p><i class="fa fa-phone"></i> (11) 4094-2059</p>
                    <p>
                        <a href="#" class="btn btn-primary">Ajudar!</a> <a href="https://www.google.com.br/maps/dir/''/R.+Purús,+475+-+Taboão,+Diadema+-+SP,+09932-240/data=!4m5!4m4!1m0!1m2!1m1!1s0x94ce44cf54e1c86f:0xde23d1a6d6119a03?sa=X&ved=0ahUKEwi97tKo_dfMAhWBkJAKHZSSBeUQiBMIKzAD" class="btn btn-default">Mais Info</a>
                    </p>
                </div>
            </div>
        </div>
        </div>
    </section>