<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="a wz"></i> <span class="light">Adot</span>Dog
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                   <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
            <?php include('includes/nav.php'); ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">A "." Dog</h1>
                        <p class="intro-text">Adote você também.<br>Isso pode mudar o mundo, isso muda você.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <?php include('includes/about.php'); ?>

    <!-- Download Section -->
    <?php include('includes/download.php'); ?>

    <!-- list Section -->
    <?php include('includes/list.php'); ?> 

    <!-- Destaque Section -->
    <?php include('includes/destaque.php'); ?> 

    <!-- Contact Section -->
    <?php include('includes/contact.php'); ?>
    <!-- Blog Section -->
    
    <?php include('includes/blog.php'); ?> 
    
