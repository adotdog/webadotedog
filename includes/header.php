<?php 
    $companyName = "AdotDog Inc";
    include ('includes/arrays.php');

    include ('includes/process.php');
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

<title><?php echo TITLE; ?></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Luiz Fernando Santiago">

    <title>AdotDog - Começar a adotar é fácil</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/toolkit-minimal.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/adotdog.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--ID Google Analytics-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-77688523-1', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- <script>
        $(document).ready(function () {
            $("button#btnsubmit").click(function(){
                
                $('#formcontact').submit();


                // $.ajax({
                //     type: "POST",
                //     url: "includes/process.php", // 
                //     data: $('form.formcontact').serialize(),
                //     success: function(msg){
                //         console.log("Bla bla")
                       
                //         console.log("Bla3 bla3 Bla3 Bla3 Bla3 Bla3")   
                //     },
                //     error: function(){
                //         alert("failure");
                //     }
                // });
            });
        });
    </script> -->
    
</head>