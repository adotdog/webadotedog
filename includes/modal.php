 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div><!-- /.modal-Header -->
                    <div class="modal-body">
                        
                        <form class="form-horizontal" method="POST" action="includes/process.php" name="formcontact" id="formcontact">
                        <fieldset>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i  class="fa fa-user bigicon"></i></span>
                                <div class="col-md-8">
                                <!-- <label for="fname"> First Name</label> -->
                                <input id="name" name="name" type="text" placeholder="Seu nome:" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                                <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Seu Email: ex:....@gmail.com" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                <div class="col-md-8">
                                <input id="phone" name="phone" type="text" placeholder="Seu telefone (opicional)" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                                <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Coloque sua mensagem aqui para nós. Nós entraremos em contato dentre 2 dias úteis." rows="4"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-check-circle bigicon"></i></span>
                                <div class="col-sm-8 col-sm-offset-0 text-left checkbox">
                                    <label><input type="checkbox" id="subscribe" name="subscribe" value="subscribe"> Gostaria de receber email com as novidades da nossa newsletter!</label>
                                </div>
                            </div>    
                        </fieldset>
                        <div class="modal-footer">
                        <button type="submit" name="formcontact_submit" value="Send Message" class="btn btn-primary" id="formcontact_submit">Enviar Mensagem!</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                    </div><!-- /.modal-Body -->
                    <!-- /.modal-footer -->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->