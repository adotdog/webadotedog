<section id="download" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Quero adotar!</h2>
                    <p>Veja a lista das Ongs parceiras para entrar em contato.</p>
                    <a href="#list" class="btn btn-list btn-lg page-scroll">Lista Completa</a>
                </div>
            </div>
        </div>
    </section>