<section id="blog" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Em breve nosso blog!</h2>
                <p>Em breve vamos colocar várias dicas e contar várias histórias para mostrar que é sim possivel dar uma nova vida aos animais!</p>
                <!--<p><a href="mailto:feedback@adotdog.com">feedback@adotdog.com</a>-->
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <!--<a href="https://twitter.com/" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>-->
                    </li>
                    <li>
                        <a href="#blog" class="btn btn-default btn-lg"><i class="fa fa-paw fa-fw"></i> <span class="network-name">AdotDog Blog</span></a>
                    </li>
                    <li>
                        <!--<a href="https://plus.google.com/" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>-->
                    </li>
                </ul>
            </div>
        </div>
    </section>