<!--      NOTE:
        In the form in contact.php, the name text field has the name "name"
        If the user submits the form, the $_POST['name'] variable will be
        automatically created, and will contain the text they typed into
        the field. The $_POST['email'] variable will contain whatever they typed
        into the email field.
    
    
        PHP used in this script:
        
        preg_match()
        - Perform a regular expression match
        - http://ca2.php.net/preg_match
        
        isset()
        - Determine if a variable is set and is not NULL
        - http://ca2.php.net/manual/en/function.isset.php
        
        $_POST
        - An associative array of variables passed to the current script via the HTTP POST method.
        - http://www.php.net/manual/en/reserved.variables.post.php
        
        trim()
        - Strip whitespace (or other characters) from the beginning and end of a string
        - http://www.php.net/manual/en/function.trim.php
        
        exit
        - Output a message and terminate the current script
        - http://www.php.net//manual/en/function.exit.php
        
        die()
        - Equivalent to exit
        - http://ca1.php.net/manual/en/function.die.php
        
        wordwrap()
        - Wraps a string to a given number of characters
        - http://ca1.php.net/manual/en/function.wordwrap.php
        
        mail()
        - Send mail
        - http://ca1.php.net/manual/en/function.mail.php -->
<section id="contact" class="container content-section text-center">
    <div class="container">

    

        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contato com a AdotDog</h2>
                <div class="col-md-12">
                    <div class="well well-sm">
                        <p>Sinta-se a vontade para enviar um e-mail e fornecer algum feedback sobre nossa página, ou sugestões de novos parceiros e Ongs que ainda não estão aqui, ou ainda que seja só para dizer Olá! :)</p>
                        
                        <p>Diretamente através do <a href="mailto:feedback@adotdog.com">feedback@adotdog.com</a>
                        <br></p>
                        <!-- Button trigger modal -->
                        <p>Ou através do <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">formulário</button></p>
                        <br>
                    </div>            
                </div>
            </div>
        </div>
   
         <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <?php 
                // Check for Header Injections
                function has_header_injection($str) {
                    return preg_match( "/[\r\n]/", $str );
                }
                    if (isset($_POST['contact_submit'])) {
                    
                    // Assign trimmed form data to variables
                    // Note that the value within the $_POST array is looking for the HTML "name" attribute, i.e. name="email"
                    $name   = trim($_POST['name']);
                    $email  = trim($_POST['email']);
                    $phone  = trim($_POST['phone']);
                    $msg    = $_POST['message']; // no need to trim message
                
                    // Check to see if $name or $email have header injections
                    if (has_header_injection($name) || has_header_injection($email)) {
                        
                        die(); // If true, kill the script
                        
                    }
              

                if (!$name || !$email || !$msg) {
                        echo '<h4 class="error">All fields required.</h4><a href="contact.php" class="button block">Go back and try again</a>';
                        exit;
                }

                // Add the recipient email to a variable
                $to = "feedback@adotdog.com";
                    
                // Create a subject
                    $subject = "$name sent a message via your contact form";
                    
                    // Construct the message
                    $message .= "Name: $name\r\n";
                    $message .= "Email: $email\r\n\r\n";
                    $message .= "Phone: $phone\r\n\r\n";
                    $message .= "Message:\r\n$msg";

                    // If the subscribe checkbox was checked
                    if (isset($_POST['subscribe']) && $_POST['subscribe'] == 'Subscribe' ) {
                    
                        // Add a new line to the $message
                        $message .= "\r\n\r\nPlease add $email to the mailing list.\r\n";
                        
                    }
                
                    $message = wordwrap($message, 72); // Keep the message neat n' tidy
                
                    // Set the mail headers into a variable
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
                    $headers .= "From: " . $name . " <" . $email . ">\r\n";
                    $headers .= "X-Priority: 1\r\n";
                    $headers .= "X-MSMail-Priority: High\r\n\r\n";
                    
                    // Send the email!
                    mail($to, $subject, $message, $headers);

                    ?>
                <!-- Show success message after email has sent -->
                <h5>Obrigado por mandar seu contato para o AdotDog</h5>
                <p>Por favor, aguarde 24 horas para uma resposta.</p>
                // <p><a href="/webadotedog/#contact" class="button block">&laquo; Recarregar a pagina</a></p>
                <?php
                    } else {
                ?>
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div><!-- /.modal-Header -->
                    <div class="modal-body">
                        <form class="form-horizontal" method="post">
                        <fieldset>
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i  class="fa fa-user bigicon"></i></span>
                                <div class="col-md-8">
                                <!-- <label for="fname"> First Name</label> -->
                                <input id="name" name="name" type="text" placeholder="Seu nome:" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                                <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Seu Email: ex:....@gmail.com" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                <div class="col-md-8">
                                <input id="phone" name="phone" type="text" placeholder="Seu telefone (opicional)" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                                <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Coloque sua mensagem aqui para nós. Nós entraremos em contato dentre 2 dias úteis." rows="4"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-check-circle bigicon"></i></span>
                                <div class="col-sm-8 col-sm-offset-0 text-left checkbox">
                                    <label><input type="checkbox" id="subscribe" name="subscribe" value="subscribe"> Gostaria de receber email com as novidades da nossa newsletter!</label>
                                </div>
                            </div>    
                        </fieldset>
                    </form>
                    </div><!-- /.modal-Body -->
                    <div class="modal-footer">
                        <button type="submit" name="contact_submit" value="Send Message" class="btn btn-primary">Enviar Mensagem!</button>
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div><!-- /.modal-footer -->
                 
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            <?php
        }
        ?>
        </div><!-- /.modal -->



        <div class="row">
                
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <!--<a href="https://twitter.com/" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>-->
                    </li>
                    <li>
                        <a href="https://www.facebook.com/AdotDog-216101805441566/" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
                    </li>
                    <li>
                        <!--<a href="https://plus.google.com/" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>-->
                    </li>
                </ul>
        </div>
         <style>
            .header {
                color: #36A0FF;
                font-size: 27px;
                padding: 10px;
            }

            .bigicon {
                font-size: 35px;
                color: #FFFFFF;
            }
        </style>
        
    </div>
</section>